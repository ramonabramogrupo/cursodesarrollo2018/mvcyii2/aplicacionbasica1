<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller{
    public function actionIndex(){
        return $this->render('index');
    }
    
    public function actionEstamos(){
        return $this->render('dondeestamos');
    }
    
    public function actionConocenos(){
        return $this->render('conocenos');
    }
    
    public function actionContacto(){
        
       //var_dump(Yii::$app->request->get('nombre'));
        //var_dump($_REQUEST);
        $longitud=0;
        $datos=Yii::$app->request->get();
        $vista="contacto";
        //var_dump($datos);
        
        /**
         * conocer si he pulsado el boton de enviar y por lo tanto
         * me llegan datos
         */
        if(isset($datos['boton'])){
            $longitud=strlen($datos['nombre']);
            $vista="resultado";
            //var_dump($longitud);
        }
               
        return $this->render($vista,[
           'titulo'=>'Introduce tus datos',
            'longitud'=>$longitud,
        ]);
    }
    
    public function actionContacto1(){
        

        $longitud=0;
        $datos=Yii::$app->request->get();
        
        /**
         * conocer si he pulsado el boton de enviar y por lo tanto
         * me llegan datos
         */
        if(isset($datos['boton'])){
            $longitud=strlen($datos['nombre']);
            
            return $this->render('resultado',[
                'longitud'=>$longitud,
            ]);
        }    
        
        return $this->render('contacto',[
           'titulo'=>'Introduce tus datos',
        ]);
    }
    
}
