<?php
    use yii\helpers\Html;
?>
<div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <?= Html::img('@web/imgs/2.jpg', ['alt' => 'Flor de loto']) ?>
      <div class="caption">
        <h3>Flor de Loto</h3>
      </div>
    </div>
  </div>
    <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <?= Html::img('@web/imgs/descarga.jpg', ['alt' => 'Flor de loto']) ?>
      <div class="caption">
        <h3>Flor de Loto</h3>
      </div>
    </div>
  </div>
</div>